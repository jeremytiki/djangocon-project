from django.conf.urls import url

from .views import QuestionListView
from .views import QuestionDetailView

app_name = 'polls'

urlpatterns = [
    url(r'^$', QuestionListView.as_view(), name="index"),
    url(r'^(?P<pk>[0-9]+)/?$', QuestionDetailView.as_view(), name="details"),
]
